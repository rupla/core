json.extract! course, :id, :name, :major, :year, :created_at, :updated_at
json.url course_url(course, format: :json)
