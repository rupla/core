json.extract! person, :id, :name, :surname, :title, :email, :phone, :created_at, :updated_at
json.url person_url(person, format: :json)
