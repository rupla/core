# frozen_string_literal: true

class CreateEventsResources < ActiveRecord::Migration[6.0]
  def change
    create_table :events_resources, id: false do |t|
      t.belongs_to :event
      t.belongs_to :resource
    end
  end
end
